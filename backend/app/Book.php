<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    $isbn;
    $title;
    $author;
    $description;
    $image;

    protected $table = "books";
    protected $fillable = ['isbn', 'title', 'author', 'description','image'];

    
    public function getIsbn(){
        return $isbn;
    }

    public function setIsbn($isbn){
        $this->nombre = $isbn;
    }

    public function setTitle($title){
        $this->title;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setAuthor($author){
        $this->author = $author;  
    }

    public function getAuthor(){
        return $this->author;
    }

    public function setDescription($description){
        $this->description;
    }
    
    public function getDescription(){
        return $this->description;
    }
}

?>
