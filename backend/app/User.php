<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
	use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'cpf','email', 'date_of_birth','age',
    	
    ];
    
    protected $dates = ['deleted_at'];

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }
    
    public function setcpfAttribute($cpf)
    {
    	$this->attributes['cpf'] = str_replace(array('.', '-'),'',$cpf);
    	 
    }

    public function Books(){
        return $this->belongsToMany('App\Book', 'book_library', 'user_id', 'book_id');
    }
}
